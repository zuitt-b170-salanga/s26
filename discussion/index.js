/* s26-index.js */ 

let http = require("http");
/*
	require function is a directive/function that is used to load a particular node module; in
	this case, we are trying to load the http module fr node.js
		http module - HyperText Transfer Protocol; lets nodejs trans data; a set of indiv files
		that are needed to create a component (used to establish data transfer bet applications)
*/

http.createServer(function(request, response){
	/*
		createServer() - allows creation of http server that listens to rqsts on a specified
		port & gives response back to the client; accepts a function that allows performing of 
		a task for the server
	*/
	response.writeHead(200, {"Content-Type": "text/plain"}); //200 default status code; successful status
	response.end("Hello World")
}).listen(4000);

console.log("Your server is now running at port:4000");

/*
	response.writeHead - used to set a status code for the response; 200 status code is the 
	default code for the node js since this means that the response is successfully processed

	response.end - signals the end of the response process.

	listen("4000") - the server will be assigned to the specified port using this command

	port - virtual pt where network connections start & end; each port is specific to a certain
	process/server

	Content-Type - sets the type of the content w/c will be displayed on the client

	ctrl + c - the gitbash will terminate any process being done in the directory/local repo
*/