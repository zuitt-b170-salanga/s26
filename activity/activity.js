/* s26 - activity.js */

// What directive is used by Node.js in loading the modules it needs? - ! REQUIRE DIRECTIVE

// What Node.js module contains a method for server creation? - ! createServer

// What is the method of the http object responsible for creating a server using Node.js? - 
// ! http.createServer() method

// What method of the response object allows us to set status codes and content types? - 
// ! response.writeHead 

// Where will console.log() output its contents when run in Node.js? - ! browser

// What property of the request object contains the address's endpoint? !response.end
