/* s26-index.js */

const http = require("http");

const port = 3000;

const server = http.createServer((request, response)=>{
	if (request.url === "/home") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("My Home Page");
	
	
	}else{
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("You got an error!!");
	}
});

server.listen(port);

console.log(`Server now running at localhost: ${port}`);
